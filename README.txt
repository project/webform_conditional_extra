INTRODUCTION
------------
Module provides additional conditions for webform conditional functionality.

REQUIREMENTS
------------
This module requires the following modules:
 * Webform (https://www.drupal.org/project/webform)

INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will add new conditions to condition dropdown on
node/*/webform/conditionals page.
