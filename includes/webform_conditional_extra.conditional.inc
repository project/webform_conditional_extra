<?php
/**
 * @file
 * Additional conditions to webform conditions functionality.
 */

/**
 * Implements hook_webform_conditional_operator_info().
 */
function _webform_conditional_extra_operator_info() {

  $operators['select']['amount_checked_less'] = array(
    'label' => t('amount of checked items is less than'),
    'comparison callback' => 'webform_conditional_extra_operator_select_less_than',
    'js comparison callback' => 'conditionalOperatorSelectAmountCheckedIsLessThan',
    'form callback' => 'webform_conditional_extra_form_select_number_less',
  );

  $operators['select']['amount_checked_more'] = array(
    'label' => t('amount of checked items is more than'),
    'comparison callback' => 'webform_conditional_extra_operator_select_more_than',
    'js comparison callback' => 'conditionalOperatorSelectAmountCheckedIsMoreThan',
    'form callback' => 'webform_conditional_extra_form_select_number_more',
  );

  $operators['select']['amount_checked_is'] = array(
    'label' => t('amount of checked items is'),
    'comparison callback' => 'webform_conditional_extra_operator_select_is',
    'js comparison callback' => 'conditionalOperatorSelectAmountCheckedIs',
    'form callback' => 'webform_conditional_extra_form_select_number',
  );

  $operators['select']['amount_checked_is_1_and_it_is'] = array(
    'label' => t('amount of checked items is 1 and it is'),
    'comparison callback' => 'webform_conditional_extra_operator_select_is_1_and_it_is',
    'js comparison callback' => 'conditionalOperatorSelectAmountCheckedIsAndItIs',
    'form callback' => 'webform_conditional_extra_form_select',
  );

  return $operators;
}

/**
 * Conditional callback for checking amount of checked items for less then.
 */
function webform_conditional_extra_operator_select_less_than($input_values, $rule_value) {
  return count($input_values) < (int) $rule_value;
}

/**
 * Conditional callback for checking amount of checked items for more then.
 */
function webform_conditional_extra_operator_select_more_than($input_values, $rule_value) {
  return count($input_values) > (int) $rule_value;
}

/**
 * Conditional callback for checking amount of checked items for equal.
 */
function webform_conditional_extra_operator_select_is($input_values, $rule_value) {
  foreach ($input_values as &$value) {
    if ($value == '' || $value == NULL) {
      unset($value);
    }
  }
  return count($input_values) === (int) $rule_value;
}

/**
 * Conditional callback for checking amount of checked items is 1 and it is.
 */
function webform_conditional_extra_operator_select_is_1_and_it_is($input_values, $rule_value) {
  foreach ($input_values as &$value) {
    if ($value == '' || $value == NULL) {
      unset($value);
    }
  }
  return (count($input_values) === 1) && ($input_values[0] === $rule_value);
}

/**
 * Form callback for select-type conditional fields.
 */
function webform_conditional_extra_form_select_number($node, $condition = '') {
  static $count = 0;
  $forms = array();
  webform_component_include('select');
  foreach ($node->webform['components'] as $cid => $component) {
    if (webform_component_property($component['type'], 'conditional_type') == 'select') {
      $old_options = _webform_select_options($component);
      $amount = count($old_options);
      if ($amount !== 0) {
        $options = array();
        for ($i = 1; $i <= $amount; $i++) {
          $options[$i] = $i;
        }
        switch ($condition) {
          case 'less':
            unset($options[1]);
            break;

          case 'more':
            array_pop($options);
            break;

          default:
            break;
        }
        $element = array(
          '#type' => 'select',
          '#multiple' => FALSE,
          '#size' => NULL,
          '#attributes' => array(),
          '#id' => NULL,
          '#name' => 'webform-conditional-select-' . $cid . '-' . $count,
          '#options' => $options,
          '#parents' => array(),
        );
      }
      $forms[$cid] = drupal_render($element);
    }
  }
  $count++;
  return $forms;
}

/**
 * Form callback for select-type conditional fields.
 */
function webform_conditional_extra_form_select($node) {
  return webform_conditional_form_select($node);
}

/**
 * Form callback for select-type conditional fields with less condition.
 */
function webform_conditional_extra_form_select_number_less($node) {
  return webform_conditional_extra_form_select_number($node, 'less');
}

/**
 * Form callback for select-type conditional fields with more condition.
 */
function webform_conditional_extra_form_select_number_more($node) {
  return webform_conditional_extra_form_select_number($node, 'more');
}
