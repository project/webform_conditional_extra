/**
 * @file
 * JS file related to webform functionality.
 */

(function ($) {
  'use strict';

  // Create empty webform object if not exists.
  Drupal.webform = Drupal.webform || {};

  /**
   * Comparison callback for condition - amount of checked items is more than.
   *
   * @param {object} element
   *  Element with involved in condition checking.
   * @param {object} existingValue
   *  Existing value.
   * @param {string} ruleValue
   *  Value that existingValue must be to meet condition.
   *
   * @return {boolean}
   *  If value meets condition.
   */
  Drupal.webform.conditionalOperatorSelectAmountCheckedIsMoreThan = function (element, existingValue, ruleValue) {
    existingValue = Drupal.webform.stringValue(element, existingValue);
    if (existingValue === null || typeof existingValue === 'undefined') {
      existingValue = '';
    }
    return existingValue.length > Number(ruleValue);
  };

  /**
   * Comparison callback for condition - amount of checked items is equal.
   *
   * @param {object} element
   *  Element with involved in condition checking.
   * @param {object} existingValue
   *  Existing value.
   * @param {string} ruleValue
   *  Value that existingValue must be to meet condition.
   *
   * @return {boolean}
   *  If value meets condition.
   */
  Drupal.webform.conditionalOperatorSelectAmountCheckedIs = function (element, existingValue, ruleValue) {
    existingValue = Drupal.webform.stringValue(element, existingValue);
    if (existingValue === null || typeof existingValue === 'undefined') {
      existingValue = '';
    }
    return existingValue.length === Number(ruleValue);
  };

  /**
   * Comparison callback for condition - amount of checked items is less than.
   *
   * @param {object} element
   *  Element with involved in condition checking.
   * @param {object} existingValue
   *  Existing value.
   * @param {string} ruleValue
   *  Value that existingValue must be to meet condition.
   *
   * @return {boolean}
   *  If value meets condition.
   */
  Drupal.webform.conditionalOperatorSelectAmountCheckedIsLessThan = function (element, existingValue, ruleValue) {
    existingValue = Drupal.webform.stringValue(element, existingValue);
    if (existingValue === null || typeof existingValue === 'undefined') {
      existingValue = '';
    }
    return existingValue.length < Number(ruleValue);
  };

  /**
   * Comparison callback for condition - amount of checked items is 1 and it is.
   *
   * @param {object} element
   *  Element with involved in condition checking.
   * @param {object} existingValue
   *  Existing value.
   * @param {string} ruleValue
   *  Value that @param existingValue must be to meet condition.
   *
   * @return {boolean}
   *  If value meets condition.
   */
  Drupal.webform.conditionalOperatorSelectAmountCheckedIsAndItIs = function (element, existingValue, ruleValue) {
    existingValue = Drupal.webform.stringValue(element, existingValue);
    if (existingValue === null || typeof existingValue === 'undefined') {
      existingValue = '';
    }
    return (existingValue.length === 1) && (existingValue[0] === ruleValue);
  };
})(jQuery);
